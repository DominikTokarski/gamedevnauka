﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveComponent : MonoBehaviour, IRestartableObject
{
    private Vector3 m_startPosition;
    private Quaternion m_startRotation;
    private Rigidbody2D m_rigidbody;
    public AudioSource m_audioSource;
    [SerializeField]
    private AudioClip impactSound;

    public virtual void Start()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
        GameplayManager.OnGamePaused += DoPause;
        GameplayManager.OnGamePlaying += DoPlay;
        m_startPosition = transform.position;
        m_startRotation = transform.rotation;
    }
    public void OnDestroy()
    {
        GameplayManager.OnGamePaused -= DoPause;
        GameplayManager.OnGamePlaying -= DoPlay;
    }

    void IRestartableObject.DoRestart()
    {
        Restart();
    }

    private void DoPlay()
    {
        m_rigidbody.simulated = true;
    }

    private void DoPause()
    {
        m_rigidbody.simulated = false;
    }

    public virtual void Restart()
    {
        transform.position = m_startPosition;
        transform.rotation = m_startRotation;

        m_rigidbody.velocity = Vector3.zero;
        m_rigidbody.angularVelocity = 0.0f;
        m_rigidbody.simulated = true;
       
    }

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        m_audioSource.PlayOneShot(impactSound);       
    }
}
