﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    [SerializeField]
    private Button PauseButton;
    [SerializeField]
    private Button RestartButton;
    [SerializeField]
    private TMPro.TextMeshProUGUI PointsText;
    
    void Start()
    {
        PauseButton.onClick.AddListener(delegate {
            GameplayManager.Instance.PlayPause();
        });
        RestartButton.onClick.AddListener(delegate {
            GameplayManager.Instance.Restart();
        });
    }

    public void UpdatePoints(int points)
    {
        PointsText.text = "Points: " + points;
    }
}
