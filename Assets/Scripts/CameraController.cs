﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private BallComponent followTarget;
    private Vector3 originalPosition;
    public void Init(BallComponent ballComponent)
    {
        followTarget = ballComponent;
        originalPosition = transform.position;
        StartPosition();
        
    }

    void Update()
    {
        
    }
    void LateUpdate()
    {
        if (!followTarget.IsSimulated())
        {
            return;
        }       
            transform.position = Vector3.MoveTowards(transform.position, originalPosition + followTarget.transform.position, followTarget.PhysicsSpeed());

    }

    public void StartPosition()
    {
        transform.position = followTarget.transform.position + originalPosition;
    }
}
