﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallComponent : InteractiveComponent
{
    private Rigidbody2D m_rigidbody;
    private TrailRenderer m_trailRender;
    private SpringJoint2D m_connectedJoint;
    private Rigidbody2D m_connectedBody;
    private LineRenderer m_lineRenderer;
    public float slingStart = 0.5f;
    public float maxSpringDistance;
    [SerializeField]
    private Transform point1;
    [SerializeField] 
    private Transform point2;
    private Vector3[] linePositions = new Vector3[4];
    private bool m_hitTheGround = false;
    [SerializeField]
    private AudioClip pullSound;
    [SerializeField]
    private AudioClip shootSound;
    private Animator m_animator;
    private ParticleSystem m_particles;
    [SerializeField]
    private CameraController cameraController;

    public override void Start()
    {
        base.Start();
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_animator = GetComponentInChildren<Animator>();
        m_trailRender = GetComponent<TrailRenderer>();
        m_trailRender.enabled = false;
        maxSpringDistance = 5.0f;
        m_connectedJoint = GetComponent<SpringJoint2D>();
        m_connectedBody = m_connectedJoint.connectedBody;
        m_lineRenderer = GetComponent<LineRenderer>();
        m_audioSource = GetComponent<AudioSource>();
        m_particles = GetComponentInChildren<ParticleSystem>();
        cameraController.Init(this);
    }

    void Update()
    {
        if (transform.position.x > m_connectedBody.transform.position.x + slingStart)
        {
            if(m_trailRender.enabled = !m_hitTheGround) 
            {
                m_trailRender.enabled = true;
            }
            else
            {
                m_trailRender.enabled = false;
                
            }            
            m_connectedJoint.enabled = false;
            m_lineRenderer.enabled = false;
        }
        

    }

    private void OnMouseUp()
    {
        m_rigidbody.simulated = true;
        m_audioSource.PlayOneShot(shootSound);
        m_particles.Play();
    }
    
    private void OnMouseDrag()
    {
        m_hitTheGround = false;
        m_rigidbody.simulated = false;       
        Vector3 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 newBallPos = new Vector3(worldPos.x, worldPos.y);
        float curJointDistance = Vector3.Distance(newBallPos, m_connectedBody.transform.position);
        if (curJointDistance > maxSpringDistance)
        {
            Vector2 direction = (newBallPos - m_connectedBody.position).normalized;
            transform.position = m_connectedBody.position + direction * maxSpringDistance;
        }
        else
        {
            transform.position = newBallPos;
        }
        LinePosition();
    }
    public bool IsSimulated()
    {
        return m_rigidbody.simulated;
    }
    public float PhysicsSpeed()
    {
        return m_rigidbody.velocity.magnitude;
    }


    public void LinePosition()
    {
        m_lineRenderer.positionCount = 3;
        linePositions[0] = point1.position;
        linePositions[1] = transform.position;
        linePositions[2] = point2.position;
        m_lineRenderer.SetPositions(linePositions);
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            m_hitTheGround = true;
        }
        m_animator.enabled = true;
        m_animator.Play(0);
    }

    public override void Restart()
    {
        base.Restart();
        m_connectedJoint.enabled = true;
        m_lineRenderer.enabled = true;
        m_trailRender.enabled = false;
        LinePosition();
    }

    private void OnMouseDown()
    {
        m_audioSource.PlayOneShot(pullSound);
    }

   
}

