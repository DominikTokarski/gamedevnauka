﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetComponent : InteractiveComponent
{
    private ParticleSystem m_particles;

    public override void Start()
    {
        base.Start();
        m_particles = GetComponentInChildren<ParticleSystem>();
        m_audioSource = GetComponent<AudioSource>();        
    }


    public override void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Ball"))
        {
            base.OnCollisionEnter2D(collision);
            m_particles.Play();
            GameplayManager.Instance.Points += 1;
        }
        
    }

}
