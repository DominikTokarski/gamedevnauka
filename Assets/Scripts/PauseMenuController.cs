﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuController : MonoBehaviour
{
    [SerializeField]
    private Button ResumeButton;
    [SerializeField]
    private Button QuitButton;
    [SerializeField]
    private Button RestartButton;
    [SerializeField]
    private GameObject Panel;
    [SerializeField]
    private Canvas HUD;
    [SerializeField]
    private GameObject ball;
    void Start()
    {
        ResumeButton.onClick.AddListener(delegate { OnResume(); });
        QuitButton.onClick.AddListener(delegate { OnQuit(); });
        RestartButton.onClick.AddListener(delegate {
            OnResume();
            GameplayManager.Instance.Restart();
        });

        SetPanelVisible(false);

        GameplayManager.OnGamePaused += OnPause;
    }

    void Update()
    {
        
    }

    public void SetPanelVisible(bool visible)
    {
        Panel.SetActive(visible);
        
    }

    private void OnPause()
    {
        SetPanelVisible(true);
        HUD.enabled = false;
        ball.SetActive(false);
    }

    private void OnResume()
    {
        GameplayManager.Instance.GameState = EGameState.Playing;
        SetPanelVisible(false);
        HUD.enabled = true;
        ball.SetActive(true);
    }

    private void OnQuit()
    {
        Application.Quit();
    }
}
